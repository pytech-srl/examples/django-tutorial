from django.db import models


class Question(models.Model):
    q_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField()

    def __str__(self):
        return f"{self.pub_date.strftime('%Y-%m-%d %H-%M-%S')} - {self.q_text}"


class Choice(models.Model):
    question = models.ForeignKey(
        Question,
        on_delete=models.CASCADE,
        related_name="choices",
    )
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return f"{self.question.q_text} - {self.choice_text} ({self.votes})"
