from django.urls import path
from . import views

urlpatterns = [
    path(
        "",
        views.QuestionListView.as_view(),
        name="question_list",
    ),
    # http://127.0.0.1:8000/polls/question/1/
    path(
        "question/<int:pk>/",
        views.QuestionDetailView.as_view(),
        name="question_detail",
    ),
]
